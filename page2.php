<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Trang 2</title>

    <style>
    .background {
        width: 34rem;
        border: solid 2px #4e7aa3;
        margin: auto;
        margin-top: 2rem;
        padding: 0.6rem 0.8rem;
    }

    .question {
        font-weight: bold;
    }

    .answerOption {
        margin-bottom: 1rem;
    }

    .btn {
        display: flex;
        justify-content: flex-end;
        align-items: center;
    }

    .btnSubmit {
        height: 2rem;
        width: 4.8rem;
        margin-top: 1rem;
        background-color: #5b9bd5;
        border-radius: 5px;
        border: solid 2px #385e8b;
        cursor: pointer;
        color: white;
    }
    </style>
</head>

<body>
    <?php
	 $question = array(
        "6" => array(
          "title" => "Một cửa hàng ngày đầu bán được 64 tấn gạo, ngày thứ hai bán được ít hơn ngày đầu 4 tấn .Hỏi cả hai ngày bán được bao nhiêu ki lô gam gạo?",
          "result" => "C",
          "name" => "Q6",
          "anwser" => array(
            "A" => "124 kg",
            "B" => "256 kg",
            "C" => "124000 kg",
            "D" => "60000 kg"
          ),
        ),
        "7" => array(
          "title" => "Thực hiện phép tính: 57369 + 8264 + 13725",
          "result" => "D",
          "name" => "Q7",
          "anwser" => array(
            "A" => "69348",
            "B" => "78348",
            "C" => "78258",
            "D" => "79358"
          ),
        ),
        "8" => array(
          "title" => "Mỗi thùng bút xanh có 1285 cái bút xanh. Mỗi thùng bút đỏ có 1575 cái bút đỏ. 3 thùng bút xanh và 7 thùng bút đỏ có tất cả số cái bút là:",
          "result" => "C",
          "name" => "Q8",
          "anwser" => array(
            "A" => "3855",
            "B" => "11025",
            "C" => "14880",
            "D" => "754219"
          ),
        ),
        "9" => array(
          "title" => "8 tấn 8kg = ... kg. Số thích hợp điền vào chỗ chấm là:",
          "result" => "D",
          "name" => "Q9",
          "anwser" => array(
            "A" => "88",
            "B" => "808",
            "C" => "880",
            "D" => "8008"
          ),
        ),
        "10" => array(
          "title" => "Có 2 xe ô tô màu xanh, mỗi xe chở được 3800kg gạo và 3 xe ô tô màu đỏ, mỗi xe chở được 4125kg gạo. Trung bình mỗi xe chở được số ki-lô-gam gạo là:",
          "result" => "D",
          "name" => "Q10",
          "anwser" => array(
            "A" => "7600",
            "B" => "19975",
            "C" => "12375",
            "D" => "3995"
          ),
        ),
      );
	?>

    <fieldset class="background">

        <?php
        $data = array();
        $error_data = false;
        $cookie = 0;
        $cookie_name = "result_page2";
        if (!empty($_POST['btnSubmit'])) {
            $data['Q6'] = isset($_POST['Q6']) ? $_POST['Q6'] : '';
            $data['Q7'] = isset($_POST['Q7']) ? $_POST['Q7'] : '';
            $data['Q8'] = isset($_POST['Q8']) ? $_POST['Q8'] : '';
            $data['Q9'] = isset($_POST['Q9']) ? $_POST['Q9'] : '';
            $data['Q10'] = isset($_POST['Q10']) ? $_POST['Q10'] : '';
            foreach ($data as $key => $value) {
            if (empty($value)) {
                $error_data = true;
            }
            }
            if ($error_data == true) {
                echo "<div style='color: red;'>Chưa trả lời hết câu hỏi.</div>";
            } 
            else {
                foreach ($question as $key => $value) {
                    $name = "Q" . $key;
                    if ($value['result'] == $data[$name]) {
                        $cookie = $cookie + 1;
                    }
                }
                setcookie($cookie_name, $cookie, time() + (86400 * 30), "/");
                header("Location: ./page3.php");
            }
        }
        ?>

        <form action="page2.php" method="POST" id="form" enctype="multipart/form-data">
            <div class="container">
                <?php
                foreach ($question as $key => $value) {
                    echo '<p class="question">Câu ' . $key . ': ' . $value['title'] . '</p>';
                    foreach ($value['anwser'] as $keyAnwer => $valueAnwer) {
                        echo '<div class="answerOption">
                            <input type="radio" id="' . $key . '' . $keyAnwer . '" name="' . $value['name'] . '" value="' . $keyAnwer . '">
                            <label for="' . $key . '' . $keyAnwer . '">' . $valueAnwer . '</label>
                        </div>';
                    }
                }
                ?>
            </div>
            <div class="btn">
                <input type="submit" value="Nộp bài" class="btnSubmit" name="btnSubmit" />
            </div>
        </form>
    </fieldset>



</body>

</html>