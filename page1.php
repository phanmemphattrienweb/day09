<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Trang 1</title>

    <style>
    .background {
        width: 34rem;
        border: solid 2px #4e7aa3;
        margin: auto;
        margin-top: 2rem;
        padding: 0.6rem 0.8rem;
    }

    .question {
        font-weight: bold;
    }

    .answerOption {
        margin-bottom: 1rem;
    }

    .btn {
        display: flex;
        justify-content: flex-end;
        align-items: center;
    }

    .btnSubmit {
        height: 2rem;
        width: 4.8rem;
        margin-top: 1rem;
        background-color: #5b9bd5;
        border-radius: 5px;
        border: solid 2px #385e8b;
        cursor: pointer;
        color: white;
    }
    </style>
</head>

<body>
    <?php
	 $question = array(
        "1" => array(
          "title" => "Số 7162805 được đọc là:",
          "result" => "D",
          "name" => "Q1",
          "anwser" => array(
            "A" => "Bảy mươi mốt triệu sáu mươi hai nghìn tám trăm linh năm",
            "B" => "Bảy triệu một trăm sáu mươi hai nghìn tám trăm linh năm",
            "C" => "Bảy mươi mốt triệu một trăm sáu hai nghìn không trăm tám mươi lăm",
            "D" => "Bảy triệu một trăm sáu mươi hai nghìn tám trăm không năm"
          ),
        ),
        "2" => array(
          "title" => "Chữ số 4 trong số 492357061 thuộc hàng nào?",
          "result" => "A",
          "name" => "Q2",
          "anwser" => array(
            "A" => "Hàng trăm triệu",
            "B" => "Hàng trăm nghìn",
            "C" => "Hàng chục triệu",
            "D" => "Hàng trăm"
          ),
        ),
        "3" => array(
          "title" => "Các số ở dòng nào được viết theo thứ tự từ bé đến lớn.",
          "result" => "A",
          "name" => "Q3",
          "anwser" => array(
            "A" => "567899; 567898; 567897; 567896.",
            "B" => "978653; 979653; 970653; 980653.",
            "C" => "865742; 865842; 865942; 865043.",
            "D" => "754219; 764219; 774219; 775219."
          ),
        ),
        "4" => array(
          "title" => "Trong các số sau số nào chia hết cho 2?",
          "result" => "D",
          "name" => "Q4",
          "anwser" => array(
            "A" => "1235",
            "B" => "1331",
            "C" => "2469",
            "D" => "1998"
          ),
        ),
        "5" => array(
          "title" => "Một cửa hàng trong hai ngày bán được 620 kg gạo. Hỏi trong 7 ngày cửa hàng bán được bao nhiêu ki-lô-gam gạo? (Biết rằng số gạo mỗi ngày bán được là như nhau).",
          "result" => "D",
          "name" => "Q5",
          "anwser" => array(
            "A" => "4340 kg",
            "B" => "217 kg",
            "C" => "434 kg",
            "D" => "2170 kg"
          ),
        ),
      );
	?>

    <fieldset class="background">
    <?php
    $data = array();
    $error_data = false;
    $cookie = 0;
    $cookie_name = "result_page1";
    if (!empty($_POST['btnSubmit'])) {
        $data['Q1'] = isset($_POST['Q1']) ? $_POST['Q1'] : '';
        $data['Q2'] = isset($_POST['Q2']) ? $_POST['Q2'] : '';
        $data['Q3'] = isset($_POST['Q3']) ? $_POST['Q3'] : '';
        $data['Q4'] = isset($_POST['Q4']) ? $_POST['Q4'] : '';
        $data['Q5'] = isset($_POST['Q5']) ? $_POST['Q5'] : '';
        foreach ($data as $key => $value) {
          if (empty($value)) {
            $error_data = true;
          }
        }
        if ($error_data == true) {
          echo "<div style='color: red;'>Chưa trả lời hết câu hỏi.</div>";
        } 
        else {
          foreach ($question as $key => $value) {
            $name = "Q" . $key;
            if ($value['result'] == $data[$name]) {
              $cookie = $cookie + 1;
            }
          }
          setcookie($cookie_name, $cookie, time() + (86400 * 30), "/");
          header("Location: ./page2.php");
        }
      }
    ?>

        <form action="page1.php" method="POST" id="form" enctype="multipart/form-data">
            <div class="container">
                <?php
                foreach ($question as $key => $value) {
                    echo '<p class="question">Câu ' . $key . ': ' . $value['title'] . '</p>';
                    foreach ($value['anwser'] as $keyAnwer => $valueAnwer) {
                        echo '<div class="answerOption">
                            <input type="radio" id="' . $key . '' . $keyAnwer . '" name="' . $value['name'] . '" value="' . $keyAnwer . '">
                            <label for="' . $key . '' . $keyAnwer . '">' . $valueAnwer . '</label>
                        </div>';
                    }
                }
                ?>
            </div>
            <div class="btn">
                <input type="submit" value="Next" class="btnSubmit" name="btnSubmit" />
            </div>
        </form>
    </fieldset>



</body>

</html>